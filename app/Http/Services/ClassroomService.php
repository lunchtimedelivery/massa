<?php
namespace App\Http\Services;

//require_once __DIR__ . '/../../../classrroom.php';

use Google_Client;
use Google_Service_Classroom;

class ClassroomService{ 
    static public function getClient() :Google_Client
    { 
        //return getClient();
        
        $client = new Google_Client();
        $client->setApplicationName('Google Classroom API PHP Quickstart');
        $client->setScopes(Google_Service_Classroom::CLASSROOM_COURSES_READONLY);
        $client->setAuthConfig( __DIR__ . '/../../../credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $client->setAccessToken('4/2QE4QHo_L1LHoMVgbnqTILZJPb2Id-k9SDgsF8Dckq5u-vjxRVJfgwo');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        /*
        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = '4/2QE4QHo_L1LHoMVgbnqTILZJPb2Id-k9SDgsF8Dckq5u-vjxRVJfgwo';  // trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        */
        return $client;
        
    }

    static public function service(Google_Client $client )
    { 
        return new Google_Service_Classroom($client);
    }
}